let mix = require("laravel-mix");

mix
.setPublicPath('./')
.js("resources/js/app.js", "assets/js")
.version();