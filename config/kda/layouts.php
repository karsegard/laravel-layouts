<?php
// config for KDA/Laravel\Layouts
return [
    'assets_controller'=>true,
    'assets_route'=>'layouts-assets',
    'images_controller'=>true,
    'images_route'=>'layouts-images'

];
