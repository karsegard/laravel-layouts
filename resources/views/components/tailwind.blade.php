@props([
    'bodyAttributes'=>[]
])

@php
    $bodyAttributes = new \Illuminate\View\ComponentAttributeBag($bodyAttributes);
@endphp

<x-laravel-layouts::base>
    @slot('body')
        <body {{$bodyAttributes->merge(['class'=>"h-full min-h-full relative h-screen typo-md"])}}>
            <x-laravel-layouts::body>
                {{$slot}}
            </x-laravel-layouts::body>
        </body>
    @endslot
</x-laravel-layouts::base>