@props([
    'body'=>null,
    'bodyAttributes'=>[]
])

@php
    $bodyAttributes = new \Illuminate\View\ComponentAttributeBag($bodyAttributes);
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{$attributes}}>
    <head>
        {{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('head.start') }}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {{--<link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet"/>--}}
        @livewireStyles
        {{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('head.styles') }}
        @stack('styles')
        {{--@include('kda-seo::meta')--}}
       {{-- <script src="//unpkg.com/alpinejs" defer></script>
        <script defer src="https://unpkg.com/@alpinejs/focus@3.x.x/dist/cdn.min.js"></script>--}}
        @stack('head.scripts')
        {{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('head.scripts') }}
        {{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('head.end') }}
    </head>
    @if(!blank($body))
        {{$body}}
    @else
        <body {{$bodyAttributes->merge(['class'=>''])}}>
            <x-laravel-layouts::body>
                {{$slot}}
            </x-laravel-layouts::body>
        </body>
    @endif
</html>
