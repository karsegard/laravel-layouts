{{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('body.start') }}
{{ $slot }}
{{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('body.after') }}
@stack('scripts')
@livewire('livewire-ui-modal')
@stack('before_livewire_scripts')
@livewireScripts
@stack('after_livewire_scripts')
<script src="{{ asset(mix('js/app.js')) }}"></script>
@stack('livewire_scripts')

{{ \KDA\Laravel\Layouts\Facades\LayoutManager::renderHook('body.end') }}