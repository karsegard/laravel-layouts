
@foreach (\KDA\Laravel\Layouts\Facades\LayoutManager::getStyles() as $name => $path)
            @if (\Illuminate\Support\Str::of($path)->startsWith(['http://', 'https://']))
                <link href"{{ $path }}" rel="stylesheet">
            @elseif (\Illuminate\Support\Str::of($path)->startsWith('<'))
                {!! $path !!}
            @else
                <link href="{{ route(config('kda.laravel-layouts.assets_route'), [
                    'file' => "{$name}.css",
                ]) }}" rel="stylesheet">
            @endif
@endforeach