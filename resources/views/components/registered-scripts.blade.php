@foreach (\KDA\Laravel\Layouts\Facades\LayoutManager::getScripts() as $name => $path)
            @if (\Illuminate\Support\Str::of($path)->startsWith(['http://', 'https://']))
                <script defer src="{{ $path }}"></script>
            @elseif (\Illuminate\Support\Str::of($path)->startsWith('<'))
                {!! $path !!}
            @else
                <script defer src="{{ route(config('kda.laravel-layouts.assets_route'), [
                    'file' => "{$name}.js",
                ]) }}"></script>
            @endif
@endforeach