<?php
namespace KDA\Laravel\Layouts;

use KDA\Laravel\Layouts\Commands\MakeLayoutCommand;
use KDA\Laravel\PackageServiceProvider;
use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Layouts\Facades\LayoutManager as Facade;
use KDA\Laravel\Layouts\LayoutManager as Library;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasComponentNamespaces;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasViews;
use Illuminate\Support\Facades\Route;
use KDA\Laravel\Layouts\Facades\LayoutManager;
use KDA\Laravel\Layouts\Http\Controllers\AssetController;
use KDA\Laravel\Layouts\Http\Controllers\ImageAssetController;
use KDA\Laravel\Traits\HasHelper;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasViews;
    use HasComponentNamespaces;
    use HasCommands;
    use HasHelper;
    protected $packageName ='laravel-layouts';


    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    protected $_commands = [
        MakeLayoutCommand::class
    ];
    protected $configDir='config';
    protected $configs = [
         'kda/layouts.php'  => 'kda.laravel-layouts'
    ];

    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister(){
       
    }
    //called after the trait were booted
    protected function bootSelf(){
      
        if(config('kda.laravel-layouts.assets_controller',false)){
            Route::get('/assets/{file}', AssetController::class)->where('file', '.*')->name(config('kda.laravel-layouts.assets_route','asset'));
        }

        if(config('kda.laravel-layouts.images_controller',false)){
            Route::get('/images/{file}', ImageAssetController::class)->where('file', '.*')->name(config('kda.laravel-layouts.images_route','images'));
        }
        
        LayoutManager::registerRenderHook(
            'head.scripts',
            function(){
               return  Blade::render('<x-laravel-layouts::registered-scripts/>');
            }
        );

        LayoutManager::registerRenderHook(
            'head.styles',
            function(){
               return  Blade::render('<x-laravel-layouts::registered-styles/>');
            }
        );
        LayoutManager::registerScripts([
            'laravel-layouts'=>__DIR__.'/../assets/js/app.js'
        ]);
    }
}
