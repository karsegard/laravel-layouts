<?php

namespace KDA\Laravel\Layouts\Facades;

use Illuminate\Support\Facades\Facade;

class LayoutManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
