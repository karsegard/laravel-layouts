<?php

namespace KDA\Laravel\Layouts\Http\Controllers;

use Illuminate\Support\Str;
use KDA\Laravel\Layouts\Facades\LayoutManager;

class ImageAssetController
{
    public function __invoke(string $file)
    {
      
//dump($file);
       
        if (Str::endsWith($file, ['.gif','.jpg','.png'])) {
            $ext = Str::afterLast($file,'.');
            $path = (string)Str::of($file)->dirname();
            $name = (string)Str::of($file)->basename('.'.$ext);
            /*dump($path,$name,$ext);
            abort_unless(
                array_key_exists((string)$path, LayoutManager::getImages()),
                404,
            );*/
            $imagePath = LayoutManager::getImages()[$path] ?? LayoutManager::getImages()['default'].'/'.$path;
            $filepath =$imagePath."/".$name.".".$ext;
      //      dump($path,$name,$ext,$imagePath,$filepath);
//dump(LayoutManager::getImages()[$path]."/".$name.".".$ext);
            return $this->pretendResponseIsFile($filepath, 'image/'.$ext.';');
        }

        abort(404);
    }

    protected function getHttpDate(int $timestamp)
    {
        return sprintf('%s GMT', gmdate('D, d M Y H:i:s', $timestamp));
    }

    protected function pretendResponseIsFile(string $path, string $contentType)
    {
        abort_unless(
            file_exists($path) || file_exists($path = base_path($path)),
            404,
        );

        $cacheControl = 'public, max-age=31536000';
        $expires = strtotime('+1 year');

        $lastModified = filemtime($path);

        if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'] ?? '') === $lastModified) {
            return response()->noContent(304, [
                'Expires' => $this->getHttpDate($expires),
                'Cache-Control' => $cacheControl,
            ]);
        }

        return response()->file($path, [
            'Content-Type' => $contentType,
            'Expires' => $this->getHttpDate($expires),
            'Cache-Control' => $cacheControl,
            'Last-Modified' => $this->getHttpDate($lastModified),
        ]);
    }
}
