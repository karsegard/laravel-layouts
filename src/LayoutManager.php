<?php
namespace KDA\Laravel\Layouts;
use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Layouts\Concerns\RendersHook;

//use Illuminate\Support\Facades\Blade;
class LayoutManager 
{
    use RendersHook;
    protected array $scripts = [];
    protected array $styles = [];
    protected array $images = [];
    
    public function render( Layout  | string  $layout,array $content, array $args = []){
        $layout = is_string($layout) ? resolve($layout): $layout;
        $component = $layout::getComponent();
        $argString = collect($args)->map(function($item,$key){
            return ":".$key."=$".$key;
        })->join("\n");
        return Blade::render(
            <<<BLADE
                <x-dynamic-component
                    :component="\$component"
                    :content="\$content"
                    {$argString}
                />
                BLADE,
            array_merge(['component' => $component, 'content' => $content],$args)
        );
    }

    public function registerScripts(array $scripts): void
    {
        $this->scripts = array_merge($this->scripts, $scripts);
    }

    public function registerStyles(array $styles): void
    {
        $this->styles = array_merge($this->styles, $styles);
    }

    public function registerImages(array $images): void
    {
        $this->images = array_merge($this->images, $images);
    }
    public function getScripts(): array
    {
        return $this->scripts;
    }
    public function getStyles(): array
    {
        return $this->styles;
    }
    public function getImages(): array
    {
        return $this->images;
    }
}