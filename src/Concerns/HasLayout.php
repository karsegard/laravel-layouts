<?php

namespace KDA\Laravel\Layouts\Concerns;


use ReflectionClass;
use  Str;
trait HasLayout{


    public function getAvailableLayoutsAttribute(){
        return static::$layouts;
    }

    public static function getAvailableLayouts():array{
        return collect(static::$layouts)
        ->mapWithKeys(fn($item)=>[$item=>$item::getLabel()])->toArray();
    }

    public function getLayoutViewAttribute(){
        return ($this->layout)::$view;
    }
}