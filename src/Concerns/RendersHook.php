<?php

namespace KDA\Laravel\Layouts\Concerns;

use Closure;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Support\HtmlString;

trait RendersHook
{
    protected array $renderHooks = [];

    public function registerRenderHook(string $name, Closure $callback): void
    {
        $this->renderHooks[$name][] = $callback;
    }

    public function renderHook(string $name): Htmlable
    {
        $debug = app()->isLocal() ? '<!--render-hook:'.$name.'-->' : '';
        $hooks = array_map(
            fn (callable $hook): string => (string) app()->call($hook).$debug,
            $this->renderHooks[$name] ?? [],
        );

        return new HtmlString($debug.implode('', $hooks));
    }
}
