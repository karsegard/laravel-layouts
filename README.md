# This is my package laravel-layouts

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-layouts.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-layouts)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-layouts.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-layouts)

## Installation


## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](../CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
